import React from 'react'
import renderer from 'react-test-renderer'

import { StationsContainer } from './stations-container'

describe('StationsContainer', () => {
  it('renders stations', () => {
    const callingPoints = [
      {
        hasDeparted: true,
        isTrainHere: false,
        station: 'Sevenoaks',
        scheduledAt: '10:00',
        estimatedAt: 'On time',
        isOrigin: true,
        isDestination: false,
        isSelected: false,
      },
      {
        hasDeparted: true,
        isTrainHere: false,
        station: 'Bat & Ball',
        scheduledAt: '10:03',
        estimatedAt: 'On time',
        isOrigin: false,
        isDestination: false,
        isSelected: true,
      },
    ]

    const tree = renderer.create(<StationsContainer callingPoints={callingPoints} />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('does not renders stations if empty', () => {
    const callingPoints = []
    const tree = renderer.create(<StationsContainer callingPoints={callingPoints} />).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
