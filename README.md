#### On this Demo
- [styled-components](https://github.com/styled-components/styled-components) - to try something new and because it's trendy (or at least it was trendy in December 2016)
- tests - using [jest snapshots](https://facebook.github.io/jest/docs/tutorial-react.html#snapshot-testing)
- linter - using [airbnb](https://www.npmjs.com/package/eslint-config-airbnb) as a guide
- [redux](https://github.com/reactjs/redux) - even thou it's not necessary for this demo
- [redux-act](https://github.com/pauldijou/redux-act) - for simple action creators and reducers (personal preference)
- network latency simulation - use chrome > network > throttling
- css-only - for the Icon I used a random SVG I found as a React Component

#### Not on this Demo
- Responsive layout - I could not see the need with the given requirements

---

# FE-exercise-contractor
JavaScript Front-end Developer Test - Contractor

Using the provided boilerplate and the data in `data/ldb.json`, create an application that displays the train service from Farringdon to West Hampstead Thameslink as shown in the following mockup:

![mockup](Farringdon_to_West_Hampstead_Thameslink___Live_Departures___Arrivals.png)

**Requirements:**
 - Unit tests

**Nice to have:**
 - Network latency simulation (async code)
 - Responsive layout
 - CSS-only solution (no images)

**We will pay attention to:**
 - Code quality and consistency
 - Naming conventions
 - Styling
 - Test quality
 - Commit history

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode (uses Jest).
