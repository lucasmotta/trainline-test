export const getCallingPoints = state => (
  state.departures.data ? state.departures.data.callingPoints : []
)

export const getOrigin = (state) => {
  const callingPoints = getCallingPoints(state)
  return callingPoints.length
    ? callingPoints.filter(station => station.isOrigin)[0].station
    : ''
}

export const getDestination = (state) => {
  const callingPoints = getCallingPoints(state)
  return callingPoints.length
    ? callingPoints.filter(station => station.isDestination)[0].station
    : ''
}

export const getOperator = state => (
  state.departures.data && state.departures.data.operator
)
