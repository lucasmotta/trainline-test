import React, { PropTypes } from 'react'
import styled from 'styled-components'

import TrainIcon from '../train-icon'

const Wrapper = styled.div`
  align-items: center;
  display: inline-flex;
  flex-direction: column;
  margin-left: 10px;
  margin-right: 14px;
  position: relative;
`

const Marker = styled.div`
  background: ${props => (props.highlight ? '#000' : '#fff')};
  border-radius: 50%;
  border: 1px solid ${props => (props.highlight ? '#000' : '#ccc')};;
  height: 12px;
  width: 12px;
`

const Line = styled.div`
  border-right: 1px solid #ccc;
  height: 40px;
`

const TrainMarker = styled.div`
  background: #48d5b5;
  color: #000;
  border-radius: 50%;
  padding: 1px 5px;
  height: 19px;
  width: 19px;
  position: absolute;
  top: ${props => (props.hasDeparted ? '50%' : 0)};
  left: 0;
  margin-left: -3px;
  margin-top: -3px;
`

const StationMarker = ({ isDestination, isOrigin, isTrainHere, hasDeparted }) => (
  <Wrapper>
    <Marker highlight={isDestination || isOrigin} />
    {!isDestination &&
      <Line />
    }
    {isTrainHere &&
      <TrainMarker hasDeparted={hasDeparted}>
        <TrainIcon />
      </TrainMarker>
    }
  </Wrapper>
)

StationMarker.propTypes = {
  hasDeparted: PropTypes.bool,
  isDestination: PropTypes.bool,
  isOrigin: PropTypes.bool,
  isTrainHere: PropTypes.bool,
}

export default StationMarker
