import React, { PropTypes } from 'react'
import styled from 'styled-components'

import TrainIcon from '../train-icon'

const Wrapper = styled.div`
  align-items: center;
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 24px;
`

const IconWrapper = styled.div`
  margin-right: 18px;
  width: 40px;
`

const Title = styled.h1`
  color: #999;
  font-size: 16px;
  font-weight: normal;
  margin: 0;
`

const TitleStrong = styled.span`
  color: #000;
  font-weight: bold;
`

const TitleStrongBlock = styled(TitleStrong)`
  display: block;
`

const Operator = styled.span`
  flex: 1 0 100%;
  font-size: 12px;
  color: #999;
  font-weight: 500;
  margin-top: 5px;
`

const Header = ({ destination, origin, operator }) => (
  <Wrapper>
    <IconWrapper>
      <TrainIcon />
    </IconWrapper>

    <div>
      <Title>
        <TitleStrongBlock>{origin}</TitleStrongBlock>
        to <TitleStrong>{destination}</TitleStrong>
      </Title>
      <Operator>Operated by {operator}</Operator>
    </div>
  </Wrapper>
)

Header.propTypes = {
  destination: PropTypes.string,
  operator: PropTypes.string,
  origin: PropTypes.string,
}

export default Header
