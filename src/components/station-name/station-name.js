import React, { PropTypes } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: inline-block;
`

const Title = styled.div`
  color: #222;
  font-size: 14px;
  font-weight: ${props => (props.highlight ? 'bold' : 'normal')};
  line-height: 1;
  opacity: ${props => (props.hasDeparted ? 0.5 : 1)}
`

const Status = styled.div`
  color: #222;
  font-size: 12px;
  margin-top: 3px;
  opacity: 0.5;
`

const StationName = ({ estimatedAt, hasDeparted, isSelected, station }) => (
  <Wrapper>
    <Title highlight={isSelected} hasDeparted={hasDeparted}>
      {station}
    </Title>
    <Status>
      {(estimatedAt !== 'On time' && hasDeparted) && 'Dept. '}
      {(estimatedAt !== 'On time' && !hasDeparted) && 'Exp. '}
      {estimatedAt}
    </Status>
  </Wrapper>
)

StationName.propTypes = {
  estimatedAt: PropTypes.string,
  hasDeparted: PropTypes.bool,
  isSelected: PropTypes.bool,
  station: PropTypes.string,
}

export default StationName
