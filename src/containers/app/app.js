import 'normalize.css'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import './app.css'
import { load } from '../../reducers/departures'
import Header from '../../components/header'
import Stations from '../../components/stations'

export class App extends Component {
  static propTypes = {
    error: PropTypes.string,
    isFetching: PropTypes.bool,
    load: PropTypes.func.isRequired,
  }

  componentWillMount() {
    this.props.load('/data')
  }

  render() {
    const { isFetching, error } = this.props

    if (isFetching) {
      return <div>Loading...</div>
    } else if (error) {
      return <div>Error loading data</div>
    }

    return (
      <div>
        <Header />
        <Stations />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  error: state.departures.error,
  isFetching: state.departures.isFetching,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  load,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)
