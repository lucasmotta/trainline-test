import React from 'react'
import renderer from 'react-test-renderer'

import StationMarker from './station-marker'

describe('StationMarker', () => {
  it('is destination', () => {
    const tree = renderer.create(<StationMarker isDestination />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('is not destination', () => {
    const tree = renderer.create(<StationMarker isDestination={false} />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('is origin', () => {
    const tree = renderer.create(<StationMarker isOrigin />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('is not origin', () => {
    const tree = renderer.create(<StationMarker isOrigin={false} />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('is not origin nor destination', () => {
    const tree = renderer.create(<StationMarker isDestination={false} isOrigin={false} />).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
