import React from 'react'
import renderer from 'react-test-renderer'

import StationScheduledTime from './station-scheduled-time'

describe('StationScheduledTime', () => {
  it('has departed', () => {
    const props = {
      hasDeparted: true,
      scheduledAt: '12:52',
    }
    const tree = renderer.create(<StationScheduledTime {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('has not departed', () => {
    const props = {
      hasDeparted: false,
      scheduledAt: '12:52',
    }
    const tree = renderer.create(<StationScheduledTime {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
