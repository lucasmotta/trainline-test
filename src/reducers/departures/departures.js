import { createReducer, createAction } from 'redux-act'

/**
 * Actions
 */
export const loadStart = createAction('LOAD_START')

export const loadSuccess = createAction('LOAD_SUCCESS')

export const loadFail = createAction('LOAD_FAIL')

export const load = (path = '/data') => (dispatch) => {
  dispatch(loadStart())

  return fetch(`http://localhost:3001${path}`)
    .then((response) => {
      if (response.status >= 400) {
        throw new Error(response.statusText)
      }
      return response.json()
    })
    .then(data => dispatch(loadSuccess(data)))
    .catch(error => dispatch(loadFail(error.message)))
}

/**
 * Initial State
 */
export const initialState = {
  isFetching: false,
  error: null,
  data: null,
}

/**
 * Reducer
 */
export default createReducer({
  [loadStart]: () => ({ ...initialState, isFetching: true }),
  [loadFail]: (state, error) => ({ ...initialState, error }),
  [loadSuccess]: (state, data) => ({ ...initialState, data }),
}, initialState)
