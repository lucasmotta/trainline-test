import React, { PropTypes } from 'react'
import styled from 'styled-components'

const Time = styled.div`
  color: #222;
  display: inline-block;
  flex: 0 1 40px;
  font-size: 14px;
  line-height: 1;
  opacity: ${props => (props.hasDeparted ? 0.5 : 1)}
`

const StationScheduledTime = ({ scheduledAt, hasDeparted }) => (
  <Time hasDeparted={hasDeparted}>
    {scheduledAt}
  </Time>
)

StationScheduledTime.propTypes = {
  scheduledAt: PropTypes.string,
  hasDeparted: PropTypes.bool,
}

export default StationScheduledTime
