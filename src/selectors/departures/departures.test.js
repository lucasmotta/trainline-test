import { getOrigin, getDestination, getOperator } from './departures'

describe('Departures selectors', () => {
  const state = {
    departures: {
      data: {
        operator: 'Trainline',
        callingPoints: [
          { isOrigin: false, isDestination: false, station: 'Nothing here' },
          { isOrigin: false, isDestination: false, station: 'Try again' },
          { isOrigin: true, isDestination: false, station: 'The origin' },
          { isOrigin: false, isDestination: false, station: 'Nope' },
          { isOrigin: false, isDestination: true, station: 'The destination' },
        ],
      },
    },
  }

  it('returns the origin point', () => {
    expect(getOrigin(state)).toMatchSnapshot()
  })

  it('returns the destination point', () => {
    expect(getDestination(state)).toMatchSnapshot()
  })

  it('returns the operator', () => {
    expect(getOperator(state)).toMatchSnapshot()
  })
})
