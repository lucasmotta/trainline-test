import React from 'react'
import renderer from 'react-test-renderer'

import Station from './station'

describe('Station', () => {
  it('matches snapshot', () => {
    const props = {
      estimatedAt: '12:54',
      hasDeparted: false,
      isDestination: false,
      isOrigin: false,
      isSelected: false,
      isTrainHere: false,
      scheduledAt: '12:52',
      station: 'Paddington',
    }
    const tree = renderer.create(<Station {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
