import React, { PropTypes } from 'react'
import styled from 'styled-components'

import StationName from '../station-name'
import StationMarker from '../station-marker'
import StationScheduledTime from '../station-scheduled-time'

const Wrapper = styled.div`
  display: flex;
`

const Station = ({
  estimatedAt,
  hasDeparted,
  isDestination,
  isOrigin,
  isSelected,
  isTrainHere,
  scheduledAt,
  station,
}) => (
  <Wrapper>
    <StationScheduledTime
      hasDeparted={hasDeparted}
      scheduledAt={scheduledAt}
    />
    <StationMarker
      hasDeparted={hasDeparted}
      isDestination={isDestination}
      isOrigin={isOrigin}
      isTrainHere={isTrainHere}
    />
    <StationName
      estimatedAt={estimatedAt}
      hasDeparted={hasDeparted}
      isDestination={isDestination}
      isOrigin={isOrigin}
      isSelected={isSelected || isOrigin || isDestination}
      station={station}
    />
  </Wrapper>
)

Station.propTypes = {
  estimatedAt: PropTypes.string,
  hasDeparted: PropTypes.bool,
  isDestination: PropTypes.bool,
  isOrigin: PropTypes.bool,
  isSelected: PropTypes.bool,
  isTrainHere: PropTypes.bool,
  scheduledAt: PropTypes.string,
  station: PropTypes.string,
}

export default Station
