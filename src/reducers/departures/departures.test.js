import fetchMock from 'fetch-mock'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import reducer, { load, loadStart, loadFail, loadSuccess, initialState } from './departures'

describe('load()', () => {
  const middlewares = [thunk]
  const mockStore = configureMockStore(middlewares)

  afterEach(() => {
    fetchMock.restore()
  })

  it('successfuly loads the data', () => {
    const store = mockStore({ departures: initialState })

    fetchMock.get('*', { hello: 'world' })

    return store.dispatch(load())
      .then(() => expect(store.getActions()).toMatchSnapshot())
  })

  it('fails to load the data', () => {
    const store = mockStore({ departures: initialState })

    fetchMock.get('*', 404)

    return store.dispatch(load())
      .then(() => expect(store.getActions()).toMatchSnapshot())
  })
})

describe('loadStart()', () => {
  const expected = {
    isFetching: true,
    error: null,
    data: null,
  }

  it('updates the state', () => {
    expect(reducer(undefined, loadStart())).toEqual(expected)
  })

  it('matches the snapshot', () => {
    expect(reducer(undefined, loadStart())).toMatchSnapshot()
  })
})

describe('loadFail()', () => {
  const error = 'danger!'
  const expected = {
    error,
    isFetching: false,
    data: null,
  }

  it('updates the state', () => {
    expect(reducer(undefined, loadFail(error))).toEqual(expected)
  })

  it('matches the snapshot', () => {
    expect(reducer(undefined, loadFail(error))).toMatchSnapshot()
  })
})

describe('loadSuccess()', () => {
  const data = 'unicorns!'
  const expected = {
    data,
    isFetching: false,
    error: null,
  }

  it('updates the state', () => {
    expect(reducer(undefined, loadSuccess(data))).toEqual(expected)
  })

  it('matches the snapshot', () => {
    expect(reducer(undefined, loadSuccess(data))).toMatchSnapshot()
  })
})
