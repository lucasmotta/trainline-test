import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Station from '../../components/station'
import { getCallingPoints } from '../../selectors/departures'

export const StationsContainer = ({ callingPoints }) => (
  <div>
    {callingPoints.map((station, i) => <Station key={i} {...station} />)}
  </div>
)

StationsContainer.propTypes = {
  callingPoints: PropTypes.arrayOf(PropTypes.object),
}

const mapStateToProps = state => ({
  callingPoints: getCallingPoints(state),
})

export default connect(mapStateToProps)(StationsContainer)
