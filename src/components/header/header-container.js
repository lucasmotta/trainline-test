import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Header from './header'
import { getOrigin, getDestination, getOperator } from '../../selectors/departures'

export const HeaderContainer = props => (
  <Header {...props} />
)

HeaderContainer.propTypes = {
  destination: PropTypes.string,
  operator: PropTypes.string,
  origin: PropTypes.string,
}

const mapStateToProps = state => ({
  destination: getDestination(state),
  operator: getOperator(state),
  origin: getOrigin(state),
})

export default connect(mapStateToProps)(HeaderContainer)
