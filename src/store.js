import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'

import reducers from './reducers'

const logger = createLogger({ collapsed: true })
const initialState = {}
const store = createStore(reducers, initialState, applyMiddleware(thunk, logger))

export default store
