import React from 'react'
import renderer from 'react-test-renderer'

import StationName from './station-name'

describe('StationName', () => {
  it('is on time', () => {
    const props = {
      estimatedAt: 'On time',
      hasDeparted: false,
      isSelected: false,
      station: 'Paddington',
    }
    const tree = renderer.create(<StationName {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('is has departed', () => {
    const props = {
      estimatedAt: '12:54',
      hasDeparted: true,
      isSelected: false,
      station: 'Paddington',
    }
    const tree = renderer.create(<StationName {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('is is expected', () => {
    const props = {
      estimatedAt: '12:54',
      hasDeparted: false,
      isSelected: false,
      station: 'Paddington',
    }
    const tree = renderer.create(<StationName {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
