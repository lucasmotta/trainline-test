import { combineReducers } from 'redux'

import departures from './departures'

export default combineReducers({
  departures,
})
