import React from 'react'
import renderer from 'react-test-renderer'

import Header from './header'

describe('Header', () => {
  it('matches snapshot', () => {
    const props = {
      origin: 'London',
      destination: 'Paris',
      operator: 'Trainline',
    }
    const tree = renderer.create(<Header {...props} />).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
