import 'isomorphic-fetch'
import React from 'react'
import ReactDOM from 'react-dom'
import Promise from 'es6-promise'
import { Provider } from 'react-redux'

import App from './containers/app'
import store from './store'

Promise.polyfill()
ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('root'))
